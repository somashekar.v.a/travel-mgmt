package com.xyztravels.data.mgmt;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
public class TravelDataMgmtApplication {

	public static void main(String[] args) {
		SpringApplication.run(TravelDataMgmtApplication.class, args);
	}
}