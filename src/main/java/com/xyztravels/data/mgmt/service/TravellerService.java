package com.xyztravels.data.mgmt.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xyztravels.data.mgmt.daos.TravellersRespository;
import com.xyztravels.data.mgmt.model.Traveller;

@Service
public class TravellerService {

	@Autowired
	TravellersRespository respository;

	public Iterable<Traveller> searchTravellers(String travellerName, String gender, String phoneNumber) {
		if ((travellerName != null && !travellerName.equals("")) || (gender != null && !gender.equals(""))
				|| (phoneNumber != null && !phoneNumber.equals(""))) {
			return respository.findByTravellerNameOrGenderOrPhoneNumber(travellerName, gender, phoneNumber);
		} else {
			return respository.findAll();
		}
	}

}
