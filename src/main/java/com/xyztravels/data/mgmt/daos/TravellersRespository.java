package com.xyztravels.data.mgmt.daos;

import com.xyztravels.data.mgmt.model.Traveller;
import org.springframework.data.repository.CrudRepository;

public interface TravellersRespository extends CrudRepository<Traveller, Integer> {
	Iterable<Traveller> findByTravellerNameOrGenderOrPhoneNumber(String travellerName,String gender,String phoneNumber);
}
