package com.xyztravels.data.mgmt.controller;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.xyztravels.data.mgmt.model.Traveller;
import com.xyztravels.data.mgmt.service.TravellerService;

@Controller
public class TravelController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TravelController.class);

	@Value("${welcome.message}")
	private String message;

	@Autowired
	TravellerService service;

	@GetMapping("/")
	public String welcome(Model model) {
		model.addAttribute("message", message);
		return "search"; // view
	}

	@GetMapping("/search")
	public String search(Model model, @RequestParam(value = "travellerName", required = true) String travellerName,
			@RequestParam(value = "gender", required = false) String gender,
			@RequestParam(value = "phoneNumber", required = false) String phoneNumber) {
		model.addAttribute("message", message);
		model.addAttribute("travellers", service.searchTravellers(travellerName, gender, phoneNumber));
		return "search"; // view
	}
}
