DROP TABLE IF EXISTS travellers;
 
CREATE TABLE travellers (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  traveller_name VARCHAR(250) NOT NULL,
  gender VARCHAR(250) NOT NULL,
  age INT DEFAULT NULL,
  phone_number VARCHAR(250) DEFAULT NULL,
  id_type VARCHAR(250) NULL,
  id_number VARCHAR(250) NULL,
  date_of_birth VARCHAR(250)
);
 
/*INSERT INTO travellers values(1,'Somashekara V A','MALE',34,'9880216256','aadhar','1234-1234-1234','15/09/1984');
INSERT INTO travellers values(2,'Somashekara V A','MALE',34,'9880216256','aadhar','1234-1234-1234','15/09/1984');
INSERT INTO travellers values(3,'Somashekara V A','MALE',34,'9880216256','aadhar','1234-1234-1234','15/09/1984');
INSERT INTO travellers values(4,'Somashekara V A','MALE',34,'9880216256','aadhar','1234-1234-1234','15/09/1984');
INSERT INTO travellers values(5,'Somashekara V A','MALE',34,'9880216256','aadhar','1234-1234-1234','15/09/1984');
INSERT INTO travellers values(6,'Kavitha V','FEMALE',34,'9880216256','aadhar','1234-1234-1234','15/09/1984');
INSERT INTO travellers values(7,'Kavitha V','FEMALE',34,'9880216256','aadhar','1234-1234-1234','15/09/1984');
INSERT INTO travellers values(8,'Kavitha V','FEMALE',34,'9880216256','aadhar','1234-1234-1234','15/09/1984');
INSERT INTO travellers values(9,'Kavitha V','FEMALE',34,'9880216256','aadhar','1234-1234-1234','15/09/1984');
INSERT INTO travellers values(10,'Kavitha V','FEMALE',34,'9880216256','aadhar','1234-1234-1234','15/09/1984');*/

INSERT INTO travellers(id,traveller_name,gender,age,phone_number) values(1,'ROHIT KUMAR','MALE',21,'8749004836');
INSERT INTO travellers(id,traveller_name,gender,age,phone_number) values(2,'BASAVARAJU K S','MALE',43,'8660073349');
INSERT INTO travellers(id,traveller_name,gender,age,phone_number) values(3,'K S NAGARAJ','MALE',57,'9945098661');
INSERT INTO travellers(id,traveller_name,gender,age,phone_number) values(4,'MANJUNATH','MALE',55,'8147891998');
INSERT INTO travellers(id,traveller_name,gender,age,phone_number) values(5,'ABHISHEK K S','MALE',25,'7899365138');
INSERT INTO travellers(id,traveller_name,gender,age,phone_number) values(6,'CHETHAN B','MALE',18,'8050377381');
INSERT INTO travellers(id,traveller_name,gender,age,phone_number) values(7,'V NANDEESH','MALE',39,'9483332244');
INSERT INTO travellers(id,traveller_name,gender,age,phone_number) values(8,'S SANGANNA','MALE',72,'9663394009');
INSERT INTO travellers(id,traveller_name,gender,age,phone_number) values(9,'ANNAPPA D HULGUR','MALE',64,'9845097115');
INSERT INTO travellers(id,traveller_name,gender,age,phone_number) values(10,'JAGDISH BABU M','MALE',25,'8867885288');
INSERT INTO travellers(id,traveller_name,gender,age,phone_number) values(11,'PRUTHIVI B','MALE',23,'7411183426');
INSERT INTO travellers(id,traveller_name,gender,age,phone_number) values(12,'Diwakar prabhu','MALE',18,'8147493041');
INSERT INTO travellers(id,traveller_name,gender,age,phone_number) values(13,'UDAY KUMAR T','MALE',30,'8940820829');
INSERT INTO travellers(id,traveller_name,gender,age,phone_number) values(14,'SHEKHAR','MALE',37,'8971762502');
INSERT INTO travellers(id,traveller_name,gender,age,phone_number) values(15,'K G MAHESH','MALE',29,'9620250568');
INSERT INTO travellers(id,traveller_name,gender,age,phone_number) values(16,'G RAMYA','FEMALE',30,'8971762502');
INSERT INTO travellers(id,traveller_name,gender,age,phone_number) values(17,'SOWMYA D','FEMALE',26,'9620250568');
